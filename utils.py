import os
import platform

if "Windows" in platform.platform():
    from windows_toasts import Toast, WindowsToaster


def windows_notify(title="", msg_title="", msg_body=""):
    toaster = WindowsToaster(title)
    newToast = Toast()
    newToast.text_fields = [msg_title, msg_body]
    toaster.show_toast(newToast)


def ubuntu_notify(body):
    os.system(f'notify-send "{body}"')


def notify(title="", msg_title="", msg_body=""):
    if platform.system() == "Windows":
        windows_notify(title, msg_title, msg_body)
    elif platform.system() == "Linux":
        ubuntu_notify(msg_title + "\n" + msg_body)
