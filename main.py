import os
import platform

if "Linux" in platform.platform():
    os.environ["PYSTRAY_BACKEND"] = "appindicator"

import base64
import functools
import io
import sys
import threading
import time
from datetime import datetime, timedelta
from pathlib import Path

import pyclip
import pystray
from googletrans import Translator
from keymouse import KeyMouse
from PIL import Image
from pynput.keyboard import GlobalHotKeys, Key, Listener

from utils import notify as notification

PATH_DIR_ROOT = Path(__file__).parent


class Converter:
    def __init__(self) -> None:
        self.keymouse = KeyMouse()
        service_urls = [
            "translate.google.com",
            "translate.google.co.kr",
            "translate.google.co.jp",
            "translate.google.co.in",
            "translate.google.co.id",
            "translate.google.co.th",
            "translate.google.co.il",
            "translate.google.co.uk",
        ]
        self.translator = Translator(service_urls=service_urls)

        self.available = self.keymouse.layout.list()

    def convert_layout(self, text: str = None, automate=False, notify=False, *args, **kwargs) -> str:
        # if "win32" in sys.platform:
        #     titles = [i for i in gw.getAllTitles() if i != ""]
        #     # .pyw doesn't create window so we take first
        #     win = gw.getWindowsWithTitle(titles[0])[0]
        #     win.activate()

        if automate:
            self.keymouse.cut()
        if not text:
            text = pyclip.paste().decode()
        cur_layout = self.keymouse.layout.get()
        # available = self.keymouse.layout.list()
        dest_lang = self.available[self.available.index(cur_layout) + 1 :]
        dest_lang = dest_lang[0] if dest_lang else self.available[0]
        new_text = self.keymouse.layout.translate(text, cur_layout, dest_lang)
        pyclip.copy(new_text)
        if not self.keymouse.layout.set(dest_lang):
            print("No support")
            # self.keymouse.hotkey("cmd", "space", interval=0.1)

        if automate:
            self.keymouse.paste()
        if notify:
            notification("Layout Converter", "Converted layout", f"From '{cur_layout}' to '{dest_lang}':\n{new_text}")
        return new_text

    def convert_capitalization(self, text: str = None, automate=False, notify=False, *args, **kwargs) -> str:
        # if "win32" in sys.platform:
        #     titles = [i for i in gw.getAllTitles() if i != ""]
        #     # .pyw doesn't create window so we take first
        #     win = gw.getWindowsWithTitle(titles[0])[0]
        #     win.activate()
        if automate:
            self.keymouse.cut()
        if not text:
            text = pyclip.paste().decode()
        # Text was lower so make it capitalized
        if text == text.lower():
            new_text = text.capitalize()
        # Text was capitalized so make it upper
        elif text == text.capitalize():
            new_text = text.upper()
        # Text was upper so make it lower
        elif text == text.upper():
            new_text = text.lower()
        else:
            new_text = text.lower()

        pyclip.copy(new_text)
        if automate:
            self.keymouse.paste()

        if notify:
            notification("Layout Converter", f"Converted capitalization", f"{new_text}")
        return new_text

    def translate(self, text: str = None, automate=False, notify=False, *args, **kwargs) -> str:
        if automate:
            self.keymouse.cut()
        if not text:
            text = pyclip.paste().decode()
        lang = self.keymouse.layout.get()
        translation = self.translator.translate(text, lang)
        new_text = translation.text
        if text and text[0].islower():
            new_text = new_text.lower()
        # from mtranslate import translate
        # text = translate(text, lang)
        pyclip.copy(new_text)
        if automate:
            self.keymouse.paste()
        if notify:
            notification("Layout Converter", f"Translated to '{lang}'", f"{new_text}")
        return new_text


class KeysListener(threading.Thread):
    def __init__(self, converter: Converter, automate=False, notify=False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.daemon = True
        self.converter = converter
        self.automate = automate
        self.notify = notify

        self.previous_dt = datetime.now()
        self.pressed_list = []
        self.released_list = []
        self.caps_lock = False
        self.shift = False
        self.ctrl_l = False
        self.alt_l = False

    def run(self):
        with Listener(on_press=self.on_press, on_release=self.on_release) as self.keys_listener:
            self.keys_listener.join()

    def stop(self):
        self.keys_listener.stop()

    def on_press(self, key: Key):
        print("{0} pressed".format(key))

        if key == Key.shift_l:
            self.shift = True
        elif key == Key.ctrl_l:
            self.ctrl_l = True
        elif key == Key.alt_l:
            self.alt_l = True

        self.pressed_list.append(key)
        if len(self.pressed_list) > 10:
            self.pressed_list.pop(0)

    def on_release(self, key: Key):
        print("{0} released".format(key))
        now = datetime.now()

        if key == Key.caps_lock:
            self.caps_lock = False if self.caps_lock else True
        elif key == Key.shift_l:
            self.shift = False
        elif key == Key.ctrl_l:
            self.ctrl_l = False
            self.previous_dt = now
        elif key == Key.alt_l:
            self.alt_l = False
            self.previous_dt = now

        self.released_list.append(key)
        if len(self.released_list) > 10:
            self.released_list.pop(0)

        # * Convert Shift + Shift
        if self.released_list[-2:] == [Key.shift_l] * 2:
            if (self.previous_dt + timedelta(milliseconds=250)) > now:
                # * Caps Lock
                if self.caps_lock:
                    if self.automate:
                        time.sleep(1)
                    self.converter.convert_capitalization(automate=self.automate, notify=self.notify)
                else:
                    if self.automate:
                        time.sleep(1)
                    self.converter.convert_layout(automate=self.automate, notify=self.notify)
            self.released_list = []
            return True
        # * Translate Alt + Alt
        if self.released_list[-2:] == [Key.alt_l] * 2:
            if (self.previous_dt + timedelta(milliseconds=250)) > now:
                self.converter.translate(automate=self.automate, notify=self.notify)
            self.released_list = []
            return True
        # * Change lang Alt + Num
        elif self.alt_l:
            vk = getattr(key, "vk", None)
            if vk:
                lang = self.converter.available[vk - 48 - 1 :]
                lang = lang[0] if lang else self.converter.available[0]
                print(f"Set '{lang}'")
                if not self.converter.keymouse.layout.set(lang):
                    print("No support")
                    # self.keymouse.hotkey("cmd", "space", interval=0.1)
        else:
            pass

        # self.previous_dt = now


class Pystray(threading.Thread):
    ICON = "iVBORw0KGgoAAAANSUhEUgAAAGYAAABeCAYAAADR9mGiAAAACXBIWXMAAAsSAAALEgHS3X78AAAInElEQVR4nO2de0wURxzHf4dGhVo8kYqG1qKoFWP01GotEuAEwbdcqabWRtFStC1J8dHSRI2oNa1Eq6bGWEqo11QNWj3RagV5aX3FpniQKr7QVoqCoQYVQVDYZuaW82bvwd2xjzl2P/8cM7e797v93sz8fr/ZGVQMwwAXplwXaVXZeTCqQgx1tH8bQhimXJcGACkA0EtSq4RHDwBpqhDD37QaaBaGKdcZAWCU5BaJx0MAiFSFGIw0GucFL1qKnEQBtlfYTYEdNvFiK1Mos0ssRjHlujgaDfNiB/rOPqY4QkOjUV5OHCMIiZu6gNfww/i1wNhPKjOoRTJhKv+txa9Z+oOQtCoPHjV4036vREUyYU4WnDL/ffv2LSg0+kplCpVIJszqlQuI8qmSRzTeH8mQTJgZE3sQ5cdPmqUyhUq68mkUGieuVr7s1LENzeRvAo81cUkQ2OcpBPorrYc3YapqfSF84Rk8XrjLBF2G02ceylwCcaE1fJlPHbx1ZQWlPTokiqus2HhStM+SAt6EGfaqR31v6uGtKxv/xn3cvfz6ey1Rn5t/AaqqqszlmdMnwyv+Jte4qek5NDQ8BV9fH+jSxfQbQWNNewwcOAi2rJoMAJ23K+N18Ed9flwoWZceFAtfrssylwe97g9bkxvZkgoAUGCJMtwtOAOQpX9xLhKg4thIO5/WeUUBMdzlmaEqorx95z7sKNjiVEkDUTs/Pkxo86hFcGFCXvsPJkdFEHU557tZHYdc7a82/0TURYzxofW+CY4oAebsmCFEOSfvhtUxPxe8RJSRmFGaasFtoxVxhHmbjOpRnuzwuQBzGbWWLbsKyXM4YsoNUYRBkTw3N4bikLaMMmot3Bjog6gnMpXEhGi5siWzyDISYtdRbyiv7APJqZnEe0hEX59GkDOiCWOr1SA3+vOtt4k65CKvfO+ZrEUBsbPL6IajG2/J8RPk2LJ2ebTsWwuILQy64aaI3TbvvjMNFkTdF9MkahF9PgZlB+bGT7eq7969O2xY2p/meyUqoguTnu0H+w8es6pvamqCNbvuYWdAQURhLl7rC3NXNxN5My6/HDoOM5YWwc6j/rKXhtckJheUE/vjujfOOGfpbU+CaSPDoKj4jLmM3Ojk1FuQkxcBUWHBEKnpijPXcoM3YVAXlJJ+k3j6xRHIO8vYGAPjhj6E5d/FW6X70XXsXWvHpkT4ZGat4w/wcHjryjJyGpwWZfHCeDitD8O5MOSpZaa24JvNdaXtgQJSexnqzoKogz9yh0/uXYqF4D5wgVoAEosbhMoV3rqypNk+cOVaBNFqUAvQho+GMcN7g1bD4CkAAPsZYyTW+sUoEJ2HHwAsudoIV2/VYqfAkm/WLoZA/wedWjJV65U49FB5EQW2SMU6VYghjTajJHvgT8ExijCUImgc4wkcqYuMjDPUSdmVoaWGRkanJtaDyl6YsqZ+6IGECCcOFRSVoQ49H5TC6NR4RbXSldHDQgAoVhnq1MAKQ+WqXbG4/MyPJnPQAuVt0LacnCnX7WYVkxWPW3pBr4qvAVqpW83Wu60rS2PXvcuKpHvzaRQFocHCsDtEoECzVHqbhAe1lDfvLIfs+nE0mGMTq71kmHIdWl7d3tr3IDe6vu0A4NQeLsiFZb0l3kFjSnbDCFpbShtam5v8tAe7N4AraZxlqhDDNmcPVpniirUuG9Z50IrhLutdEUXBhNDCIFESPOZeN9dD9LkMGLhiLPStPC+pKUIKU+ppe9RMyf8W8jen4unt+59Ng+Cb+ZLZIpQwpeyWU9Rv2GamvhpOZG4lqoIv5UpmjrvCBDl4D8VDCR4lCgBElx2xqsvblwnwoEISe/gWhurN2ezSXA8VB7+3+W6sMUcSk/juyhI8ThQAGHMj3+5S+NwdG7BwYsOnMItUIYbDon8DHuhRuM/hRcL/Er/V8CUMmjendhtDRyC3+FxRHnFEbPIaovz8rPX4IzTuCmM526an8WEGZ9FcOEQcOSVxGeSOf5+oQ8KJHde4JQzbOrRs9+U5ASSX+mqT52XBibFxAD37YYEs4QooNG53ZaoQQ7Gndl9txF7cS5RDtTEAAaYNH7BAFmAB68VbRS3fqeXmepPHZUHN1I9eFAJGmoSygCukkMhWGK6nhbdHGTCBqCOEAoDrx/aI5jrL9imZx7l7iHL/oMEwJGcdUce0tsBdb29obDStCUWxzqRL2VD41oeC2+fWfIzQCD0fg5KTFV/McetcbyTUnru828RBlPkY6hhw5oDbJuHWU1Mm+FeSnzA1ZVB0ZH+HLjHlT+ETHLIbY2Iv54NlMl87ay4UTf3U4TkT712Bs+s/Npfx9EB4Eo53hEJewtRXW7nId8LmmGMXe5zFrrOBSN2gaYL80CTBTJVVV8adc0FxSsXgaKfOfTppHlHG0wQCus7yEcbGnEvXibPsHs6lZEg0sUYUuc7Bdy4IZa18hEE30XLOBd3k0yNmO3+Bbj1h6PT5RFXAbz/waSKBfIThzN/jm9ytp0vXyNWQQuIxR6CpZ9kI0xpI7hjITe07hV8wxMxLNB+Juza/YEHslY0w+cOi8Ctyj4PTD7jt6uZNXWGeSAuOX8KrjZbIKyWDuh2+fuFtHpmL3aGTaOUVx/DZ7QgjiBllqR+lKMJQiiIMpdAqDLX/A1kkjFQKw+jU6CGPfygwRQr0aK0/zV2Z5z4W5T7mpSvUCsPo1MUAMFpGLQftjBHZtjMGlQEmF5WhTsP+T2RHyz/EwtXAV9/OmGlzLxmPEIYmVIY61MX+6IJJWrb1u4TiLrsI65gs4/WiNlCEcQNGp97GdlGCoQjjJoxOnSCkOIowHSNFqG1eFGE6AOvatrcHj8adT1CE6SCsOHEOdq9Su/MJijA8wMYgkXxuLaYIwxOMTm10Ytcqp1GE4RE2kFzExxUVYXiGDUAtF9q4HPWDkpIRDja/V8fNgTkFAPwPNLTBaPrYrF0AAAAASUVORK5CYII="

    def __init__(self, converter: Converter, t_keys_listener: KeysListener, notify=False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.converter = converter
        self.t_keys_listener = t_keys_listener
        self.notify = notify
        self.daemon = True

    def run(self):
        img = Image.open(io.BytesIO(base64.b64decode(self.ICON)))
        convert_layout = functools.partial(self.converter.convert_layout, automate=False, notify=self.notify)
        convert_capitalization = functools.partial(
            self.converter.convert_capitalization, automate=False, notify=self.notify
        )
        translate = functools.partial(self.converter.translate, automate=False, notify=self.notify)
        menu = [
            pystray.MenuItem("Transliterate", lambda: convert_layout(), default=True),
            pystray.MenuItem("Capitalize", lambda: convert_capitalization()),
            pystray.MenuItem("Translate", lambda: translate()),
            pystray.MenuItem("Quit", lambda: self.stop_all_exit()),
        ]
        self.icon = pystray.Icon("Pystray", icon=img, menu=menu)
        self.icon.run()

    def stop_all_exit(self):
        if self.t_keys_listener:
            self.t_keys_listener.stop()
        # TODO On ubuntu for some reason loop is still running. Maybe start icon.run() in separate thread
        self.icon.stop()
        sys.exit()


if __name__ == "__main__":
    time.sleep(30)  # Delay before start
    notify = True
    hotkeys = False
    automate = False
    try:
        converter = Converter()
        if hotkeys:
            # t_keys_listener = KeysListener(converter=converter, automate=automate, notify=notify)
            # t_keys_listener.start()
            convert_layout = functools.partial(converter.convert_layout, automate=False, notify=notify)
            t_keys_listener = GlobalHotKeys({f"<ctrl>+<alt>+1": lambda: convert_layout()})
        else:
            t_keys_listener = None

        t_pystray = Pystray(converter=converter, t_keys_listener=t_keys_listener, notify=notify)
        t_pystray.start()

        if hotkeys:
            t_keys_listener.join()

        t_pystray.join()

    except Exception as e:
        with open(PATH_DIR_ROOT / "log.log", "w") as f:
            f.write(str(e))
