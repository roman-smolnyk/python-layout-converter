from main import Converter
import sys


if __name__ == "__main__":
    text = sys.argv[1] if sys.argv[1:] else None

    converter = Converter()
    new_text = converter.convert_layout(text=text)
    print(new_text)
    
