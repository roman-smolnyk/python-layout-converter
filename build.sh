#!/bin/bash
name="Layout Converter"
python -m PyInstaller --icon=design/icon.ico --noconsole -F main.py --name="$name"
mkdir -p exe
mv -f "dist/${name}.exe" "exe/"
rm -rf build
rm -rf dist
rm -f ./*.spec
